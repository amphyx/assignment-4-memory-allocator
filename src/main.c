#include <assert.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

static struct block_header* _get_block_at_addr(void* addr) {
  return (struct block_header*) (((uint8_t*) (addr)) - offsetof(struct block_header, contents));
}

/**
 * Тест на обычное успешное выделение памяти.
*/
void test_successful_allocation() {
  const size_t heap_size = 52;
  void* heap_addr = heap_init(heap_size);
  assert(heap_addr != NULL);

  const size_t size_to_test = 30;
  void* addr = _malloc(size_to_test);
  struct block_header* block = _get_block_at_addr(addr);
  assert(block->capacity.bytes == size_to_test);

  heap_term();
}

/**
 * Тест на освобождение одного блока из нескольких выделенных.
*/
void test_free_one_block() {
  const size_t heap_size = 1000;
  heap_init(heap_size);

  const size_t first_block_size = 50;
  void* first_block_addr = _malloc(first_block_size);
  const struct block_header* first_block = _get_block_at_addr(first_block_addr);
  
  const size_t second_block_size = 80;
  void* second_block_addr = _malloc(second_block_size);
  const struct block_header* second_block = _get_block_at_addr(second_block_addr);

  _free(second_block_addr);
  assert(!first_block->is_free);
  assert(second_block->is_free);

  heap_term();
}

/**
 * Тест на освобождение двух блоков из нескольких выделенных.
*/
void test_free_two_blocks() {
  const size_t heap_size = 1000;
  heap_init(heap_size);

  const size_t first_block_size = 50;
  void* first_block_addr = _malloc(first_block_size);
  const struct block_header* first_block = _get_block_at_addr(first_block_addr);
  
  const size_t second_block_size = 80;
  void* second_block_addr = _malloc(second_block_size);
  const struct block_header* second_block = _get_block_at_addr(second_block_addr);

  const size_t third_block_size = 50;
  void* third_block_addr = _malloc(third_block_size);
  const struct block_header* third_block = _get_block_at_addr(third_block_addr);

  _free(second_block_addr);
  _free(third_block_addr);
  assert(!first_block->is_free);
  assert(second_block->is_free);
  assert(third_block->is_free);

  heap_term();
}

/**
 * Тест, если память закончилась, новый регион памяти расширяет старый.
*/
void test_heap_grow() {
  const size_t heap_size = 50;
  void* initial_block_addr = heap_init(heap_size);

  const size_t fat_block_size = 51;
  void* fat_block_addr = _malloc(fat_block_size);
  struct block_header* fat_block = _get_block_at_addr(fat_block_addr);

  printf("---------------------------------\n");
  printf("%s debug heap:\n", __func__);
  debug_heap(stdout, initial_block_addr);
  printf("---------------------------------\n");

  assert(initial_block_addr == HEAP_START);
  assert(fat_block == HEAP_START);

  heap_term();
}

/**
 * Тест, если память закончилась, а старый регион памяти не расширить из-за другого выделенного диапазона адресов,
 * новый регион выделяется в другом месте.
*/
void test_heap_grow_with_space() {
  const size_t heap_size = REGION_MIN_SIZE;
  void* initial_block_addr = heap_init(heap_size);

  const size_t space_size = REGION_MIN_SIZE;
  void* space = mmap(
    HEAP_START + REGION_MIN_SIZE,
    REGION_MIN_SIZE,
    0,
    MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED,
    -1,
    0
  );
  assert(space != MAP_FAILED);

  const size_t next_block_size = REGION_MIN_SIZE * 2;
  void* next_block_addr = _malloc(next_block_size);
  struct block_header* next_block = _get_block_at_addr(next_block_addr);

  printf("---------------------------------\n");
  printf("%s debug heap:\n", __func__);
  debug_heap(stdout, initial_block_addr);
  printf("---------------------------------\n");

  assert(next_block_addr != HEAP_START);
  assert(next_block->capacity.bytes >= next_block_size);

  munmap(space, space_size);
  heap_term();
}


int main() {
  test_successful_allocation();
  test_free_one_block();
  test_free_two_blocks();
  test_heap_grow();
  test_heap_grow_with_space();
  printf("Все тесты пройдены ^_^\n");
  return 0;
}