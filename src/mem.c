#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#define BLOCK_MIN_CAPACITY 24

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size(size_t query) {
  return size_max(round_pages(query), REGION_MIN_SIZE);
}

static block_capacity get_block_capacity(size_t query_capacity) {
  return (block_capacity) {
    .bytes = size_max(query_capacity, BLOCK_MIN_CAPACITY)
  };
}

/**
 * Добавить к запрашиваемой памяти для аллокации кол-во байт в заголовке блока.
 * 
 * @param query запрашиваемое число байт для аллокации
 * @returns число байт с учётом заголовков блока
*/
static size_t add_block_headers_size_to_query(size_t query) {
  return size_from_capacity(get_block_capacity(query)).bytes;
}

extern inline bool region_is_invalid( const struct region* r );

static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/**
 * Аллоцировать регион памяти и инициализировать его блоком.
 *
 * @param addr адрес начала аллоцирования памяти
 * @param query размер области памяти в байтах
 * @returns структура с аллоцированным регионом памяти или `REGION_INVALID`,
 *          если не удалось аллоцировать регион.
 */
static struct region alloc_region(const void* addr, size_t query) {
  const size_t actual_size = region_actual_size(
    add_block_headers_size_to_query(query)  // Добавляем к query байты заголовка
  );

  // Вначале пытаемся аллоцировать память точно по переданному адресу
  bool is_extends = true;
  int additional_flags = (
    MAP_PRIVATE | // Закрытое  отображение с механизмом копирования при записи
    MAP_FIXED     // Не учитывать addr как подсказку: помещать отображение точно по этому адресу
  );
  void* allocated_addr = map_pages(addr, actual_size, additional_flags);
  if (allocated_addr == MAP_FAILED) {
    // Если не получилось, позволяем mmap самой выбрать адрес
    is_extends = false;  // В таком случае новый блок будет не вплотную к предыдущему
    additional_flags = MAP_PRIVATE;
    allocated_addr = map_pages(addr, actual_size, additional_flags);
    if (allocated_addr == MAP_FAILED) {  // Если и так не получилось, сдаёмся
      return REGION_INVALID;
    }
  }

  // Создаём блок размером в весь регион памяти
  block_init(
    allocated_addr,
    (block_size) { .bytes = actual_size },
    NULL  // У последнего созданного блока нет следующего
  );

  return (struct region) {
    .addr = allocated_addr,
    .size = actual_size,
    .extends = is_extends,
  };
}

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

/**
 * Получить первый непоследовательный блок, начиная поиск от переданного блока.
 * 
 * Непоследовательный блок - это блок, структура которого идёт в памяти с отрывом от других последовательных блоком.
 * 
 * @param start_block блок, с которого начинается поиск
 * @returns указатель на первый непоследовательный блок
*/
static struct block_header* _get_first_noncontinious_block(struct block_header* start_block) {
  struct block_header* curr_block = start_block;
  while (curr_block->next && blocks_continuous(curr_block, curr_block->next)) {
    curr_block = curr_block->next;
  }

  return curr_block->next;
}

/**
 * Получить суммарный размер блоков в байтах между двумя блоками, включая размеры граничных блоков.
 *
 * @param start начальный блок
 * @param end конечный блок
 * @returns суммарный размер блоков между `start` и `end` в байтах, включая размеры самих `start` и `end`
*/
static size_t _get_block_sizes_between(struct block_header* start, struct block_header* end) {
  struct block_header* curr_block = start;
  size_t result = 0;
  while (curr_block) {
    result += size_from_capacity(curr_block->capacity).bytes;
    if (curr_block == end)
      break;

    curr_block = curr_block->next;
  }

  return result;
}

void* heap_init(size_t initial) {
  const struct region region = alloc_region(HEAP_START, initial);
  if (region_is_invalid(&region))
    return NULL;

  return region.addr;
}

/**
 * Освободить всю память, выделенную под кучу.
 */
void heap_term() {
  struct block_header* curr_block = (struct block_header*) HEAP_START;

  while (curr_block != NULL) {
    // Получим первый блок, который не рядом с начальным, так как после munmap все последовательные блоки сразу сотрутся
    struct block_header* block_to_jump_after = _get_first_noncontinious_block(curr_block); 
    // Полное кол-во освобождаемых байт
    size_t block_bytes_to_free = _get_block_sizes_between(curr_block, block_to_jump_after);

    int status = munmap(curr_block, block_bytes_to_free);
    if (status != 0) {
      err("munmap вернула ненулевой статус");
    }

    curr_block = block_to_jump_after;
  }
}

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

/**
 * Проверить блок, слишком ли он большой для запрашиваемого размера, и если да, то
 * разделить его на две части.
 *
 * Первая часть будет иметь размер данных для аллокации, вторая часть - оставшееся.
 * Если размер аллокации меньше, чем `BLOCK_MIN_SIZE`, то берётся `BLOCK_MIN_SIZE`.
 * Если размер второй части меньше, чем `BLOCK_MIN_SIZE`, то блок делится не будет.
 *
 * @param block блок для проверки
 * @param query сколько требуется памяти для аллокации
 * @returns был ли разделён блок
*/
static bool split_if_too_big(struct block_header* block, size_t query) {
  if (!block_splittable(block, query)) {
    return false;
  }

  block_capacity query_capacity = get_block_capacity(query);
  block_size first_block_size = size_from_capacity(query_capacity);

  block_size current_block_size = size_from_capacity(block->capacity);
  block_size second_block_size = {
    .bytes = current_block_size.bytes - first_block_size.bytes,
  };

  // Если размер второй части меньше, чем `BLOCK_MIN_SIZE`, то блок делиться не будет
  if (second_block_size.bytes < BLOCK_MIN_CAPACITY) {
    return false;
  }

  // Вычисляем адрес второго блока как смещение относительно адреса начала первого на размер первогог блока
  void* second_block_addr = ((void*) block) + first_block_size.bytes;
  block_init(
    second_block_addr,
    second_block_size,
    block->next
  );
  struct block_header* second_block = (struct block_header*) second_block_addr;

  block->next = second_block;
  block->capacity = query_capacity;

  return true;
}


/*  --- Слияние соседних свободных блоков --- */

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

/**
 * Попытаться слить переданный блок со следующим за ним в один.
 *
 * @param block основной блок
 * @returns удалось ли слить блоки
*/
static bool try_merge_with_next(struct block_header* block) {
  struct block_header* next_block = block->next;
  if (next_block == NULL || !mergeable(block, next_block)) {
    return false;
  }

  block->next = next_block->next;
  // К вместимости первого блока добавляется весь размер сливаемого блока
  block->capacity.bytes += size_from_capacity(next_block->capacity).bytes;

  return true;
}

/**
 * Слить все блоки, начиная с переданного, пока это возможно.
 *
 * @param block блок, с которого начинается соединение блоков
*/
static void merge_while_it_possible(struct block_header* block) {
  bool is_merge_possible;
  do {
    is_merge_possible = try_merge_with_next(block);
  } while (is_merge_possible);
}

/*  --- ... ecли размера кучи хватает --- */

/**
 * Результат поиска хорошего блока.
 *
 * Типы:
 * - `BSR_FOUND_GOOD_BLOCK`: хороший блок найден (он будет в поле `block`)
 * - `BSR_REACHED_END_NOT_FOUND`: дошли до конца кучи и не нашли хорошего блока
 * - `BSR_CORRUPTED`: куча повреждена
*/
struct block_search_result {
  enum { BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED } type;
  struct block_header* block;
};

/**
 * Последовательно перебрать блоки, начиная от переданного `block`,
 * найдя в конце хороший блок для размера `sz`, либо последний блок, если
 * хороших нет.
 *
 * @param block блок, с которого начинается поиск хорошего блока
 * @param sz требуемый размер блока
 * @returns результат поиска блока с найденным хорошим блоком, либо неуспешный результат с последним блоком
*/
static struct block_search_result find_good_or_last(struct block_header* restrict block, size_t sz) {
  if(block == NULL) {
    return (struct block_search_result) {
      .type = BSR_CORRUPTED,
      .block = NULL
    };
  }

  struct block_header* curr_block = block;
  // При поиске хорошего блока мы проходимся по блокам кучи.
  while (true) {  // Условие выхода описано в конце цикла
    if (curr_block->is_free) {
      // Прежде чем решать, хороший блок или нет, объединим его со всеми идущими за ним свободными блоками.
      merge_while_it_possible(curr_block);

      // Теперь определяем, хороший ли блок
      if (block_is_big_enough(sz, curr_block)) {
        return (struct block_search_result) {
          .block = curr_block,
          .type = BSR_FOUND_GOOD_BLOCK,
        };
      }
    }

    // Выходим, когда нет следующего блока (т.е. блок последний)
    if (curr_block->next == NULL)
      break;

    curr_block = curr_block->next;
  }

  return (struct block_search_result) {
    .block = curr_block,  // В конце у нас будет последний блок
    .type = BSR_REACHED_END_NOT_FOUND,  // И мы ничего не нашли
  };
}

/**
 * Попробовать выделить память в куче, начиная с блока `block`, не пытаясь расширить кучу.
 *
 * Можно переиспользовать, как только кучу расширили.
 *
 * @param query сколько требуется памяти для аллокации
 * @param block блок, с которого начинается поиск хорошего блока
 * @returns результат поиска хорошего блока
 */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header* block) {
  if (block == NULL) {
    return (struct block_search_result) { .type = BSR_CORRUPTED };
  }
  
  struct block_search_result result = find_good_or_last(block, query);

  if (result.type != BSR_FOUND_GOOD_BLOCK) {
    return result;
  }

  // Если нашли хороший блок, отдадим максимально подходящую под запрашиваемый размер
  // его часть, разделив блок на 2 части:
  split_if_too_big(result.block, query);

  // Помечаем выделенный блок как занятый
  result.block->is_free = false;

  return result;
}

/**
 * Расширить кучу на переданное число байт.
 *
 * @param last последний блок кучи
 * @param bytes_to_add число байт, на которое надо расширить
 * @returns указатель на заголовок нового блока в добавленной области, либо `NULL`, если не
 *          удалось выделить регион памяти
*/
static struct block_header* grow_heap(struct block_header* restrict last, size_t bytes_to_add) {
  if (last == NULL) {
    return NULL;
  }

  void* addr_after_blocks = block_after(last);
  struct region new_region = alloc_region(addr_after_blocks, bytes_to_add);
  if (region_is_invalid(&new_region)) {
    return NULL;
  }

  // Новый блок аллоцирован полностью в новом регионе, поэтому можем забрать его оттуда
  struct block_header* new_block = (struct block_header*) new_region.addr;
  last->next = new_block;

  if (new_region.extends) {  // Если удалось выделить блок сразу после последнего, то сольём их в один
    bool is_merged = try_merge_with_next(last);
    if (is_merged)  // Если блоки были слиты, то новой блок стал частью старого
      new_block = last;
  }

  return new_block;
}

/**
 * Реализует основную логику `malloc` и возвращает заголовок выделенного блока.
 *
 * @param query размер области памяти в байтах
 * @param heap_start указатель на начало кучи
 * @returns указатель на структуру заголовока выделенного блока
 */
static struct block_header* memalloc(size_t query, struct block_header* heap_start) {
  block_capacity query_capacity = get_block_capacity(query);

  struct block_search_result result = try_memalloc_existing(query_capacity.bytes, heap_start);

  if (result.type == BSR_REACHED_END_NOT_FOUND) {  // Не нашли хороший блок
    // Если хороший блок не найден, try_memalloc_existing вернёт последний блок
    struct block_header* last_block = result.block;
    // Будем расширять кучу на вместимость блока + его заголовки (т.е. размер блока)
    block_size size_to_grow = size_from_capacity(query_capacity);
    // Расширяем кучу, получая новый блок
    struct block_header* new_last_block = grow_heap(last_block, size_to_grow.bytes);

    result = try_memalloc_existing(query_capacity.bytes, new_last_block);
  }

  return result.type == BSR_FOUND_GOOD_BLOCK ? result.block : NULL;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

/**
 * Реализует основную логику `free`, освобождает алоцированную память по данному адресу.
 *
 * @param mem адрес памяти, на который была совершена алокация
*/
void _free(void* mem) {
  if (!mem)
    return;

  struct block_header* header = block_get_header( mem );
  header->is_free = true;

  // После освобождения блока можно его попробовать с кем-нибудь посливать
  merge_while_it_possible(header);
}
